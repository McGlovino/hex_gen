﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexManager : MonoBehaviour
{
    public bool debugText;
    public int radius;
    float tileSize = 1.3f; //1.2 is tesselating

    public GameObject hexPrefab;
    public List<Tile> tiles = new List<Tile>();

    public void GenerateMap()
    {
        Vector3 position = Vector3.zero;

        for (int q = -radius; q <= radius; q++)
        {
            int r1 = Mathf.Max(-radius, -q - radius);
            int r2 = Mathf.Min(radius, -q + radius);
            for (int r = r1; r <= r2; r++)
            {
                GameObject draw = hexPrefab;
                Tile tile = new Tile(0,0,0);

                position.x = (tileSize * 2.5f) / 2.0f * q;
                position.z = (tileSize * Mathf.Sqrt(2.0f)) * (r + q / 2.0f);

                tile.setGrid(q, r, (-q - r));
                tile.pos = position;
                if(debugText == true)
                    draw.GetComponentInChildren<TextMesh>().text = (q + "," + r + "," + (-q - r).ToString());
                tile.Object = Instantiate(draw, position, transform.rotation, this.transform) as GameObject;
                tiles.Add(tile);
            }
        }
    }

    public void Show(Tile show)
    {
        if(!show.Object.activeSelf)
            show.Object.SetActive(true);
    }

    public void Reveal(Tile current, int layers)
    {
        List<Tile> toShow = new List<Tile>();
        int toShowCount = 1;
        int doneCount = 0;

        toShow.Add(current);

        for (int i = 0; i < layers; i++)
        {
            for (int j = doneCount; j < toShowCount; j++)
            {
                for (int k = 0; k < 6; k++)
                {
                    foreach (Tile t in tiles)
                    {
                        if (toShow[j].direction(k) == t)
                        {
                            if (!t.Object.activeSelf)
                                t.Object.SetActive(true);
                            toShow.Add(t);
                            break;
                        }
                    }
                }
            }
            doneCount = toShowCount;
            toShowCount = toShow.Count;
        }
    }

    public Tile findTile(GameObject id)
    {
        foreach (Tile t in tiles)
        {
            if (t.Object == id)
                return t;
        }
        return null;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile
{
    public int q, r, s;
    public Vector3 pos;
    public GameObject Object;

    public Tile(int _q, int _r, int _s)
    {
        q = _q;
        r = _r;
        s = _s;

        setGrid(q,r,s);
    }


    /*private List<Tile> directions = new List<Tile>(){
        new Tile(1, 0, -1), new Tile(1, -1, 0), new Tile(0, -1, 1),
        new Tile(-1, 0, 1), new Tile(-1, 1, 0), new Tile(0, 1, -1)
    };

    public Tile direction(int direction)//0-5
    {
        if (direction < 6 && direction >= 0)
        {
            return directions[direction];
        }
        else
            return null;
    }*/

    private List<Vector3> directions = new List<Vector3>(){
        new Vector3(1, 0, -1), new Vector3(1, -1, 0), new Vector3(0, -1, 1),
        new Vector3(-1, 0, 1), new Vector3(-1, 1, 0), new Vector3(0, 1, -1)
    };

    public Tile direction(int direction)//0-5
    {
        if (direction < 6 && direction >= 0)
        {
            return new Tile((int)(q + directions[direction].x), (int)(r + directions[direction].y), (int)(s + directions[direction].z));
        }
        else
            return null;
    }

    public void setGrid(int _q, int _r, int _s)
    {
        q = _q;
        r = _r;
        s = _s;
    }

    public void setGrid(int x, int y) {
        q = y - (x - (x % 2)) / 2;
        r = x;
        s = -q - r;
    }

    public Vector3 getGrid()
    {
        return new Vector3(q,r,s);
    }

    int length()
    {
        return ((q+r+s)/2);
    }

    int distance(Tile a, Tile b)
    {
        return (a-b).length();
    }

    public static Tile operator +(Tile a, Tile b)
    {
        return new Tile(a.q +b.q, a.r + b.r, a.s + b.s);
    }

    public static Tile operator -(Tile a, Tile b)
    {
        return new Tile(a.q - b.q, a.r - b.r, a.s - b.s);
    }

    public static Tile operator *(Tile a, int b)
    {
        return new Tile(a.q *b, a.r *b, a.s *b);
    }

    public static bool operator ==(Tile a, Tile b)
    {
        return a.q == b.q && a.r == b.r && a.s == b.s;
    }

    public static bool operator !=(Tile a, Tile b)
    {
        return !(a == b);
    }

}

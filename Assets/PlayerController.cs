﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject HManager;
    Tile currentTile;
    Graph g;
    int currentWP = 0;

    float speed = 8.0f;
    int revealDistance = 3;

    public void Init()
    {
        g = HManager.GetComponent<WPManager>().graph;
        StartPos();
    }

    void StartPos()
    {
        foreach (Tile t in HManager.GetComponent<HexManager>().tiles)
        {
            if (t == new Tile(0, 0, 0))
            {
                currentTile = t;
                moveTo(t.pos);
                HManager.GetComponent<HexManager>().Show(t);
                HManager.GetComponent<HexManager>().Reveal(t, revealDistance);
                break;
            }
        }
    }

    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 800))
            {
                foreach (Tile t in HManager.GetComponent<HexManager>().tiles)
                {
                    if (hit.transform.gameObject == t.Object)
                    {
                        g.AStar(currentTile.Object, hit.transform.gameObject);
                        currentWP = 0;
                        break;
                    }
                }
            }
        }
    }

    private void LateUpdate()
    {
        if (g.getPathLength() == 0 || currentWP == g.getPathLength())
            return;

        currentTile = HManager.GetComponent<HexManager>().findTile(g.getPathPoint(currentWP));

        if (this.transform.position.x == g.getPathPoint(currentWP).transform.position.x && this.transform.position.z == g.getPathPoint(currentWP).transform.position.z)
        {
            currentWP++;
        }

        if (currentWP < g.getPathLength())
        {
           moveTo(g.getPathPoint(currentWP).transform.position);
        }
    }

    public void moveTo(Vector3 pos)
    {
        Vector3 direction = new Vector3(pos.x, this.transform.position.y, pos.z) - this.transform.position;
        if (direction.normalized.magnitude > direction.magnitude)
            this.transform.position += direction.normalized * Time.deltaTime * speed;
        else
            this.transform.position = new Vector3(pos.x, this.transform.position.y ,pos.z);
        HManager.GetComponent<HexManager>().Reveal(currentTile, revealDistance);
    }
}

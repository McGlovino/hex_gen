﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WPManager : MonoBehaviour
{
    public Graph graph = new Graph();

    HexManager manager;

    public void GenerateLinks()
    {
        manager = this.GetComponent<HexManager>();

        for (int i = 0; i < manager.tiles.Count; i++)
        {
            graph.AddNode(manager.tiles[i].Object, manager.tiles[i]);
        }

        for (int i = 0; i < manager.tiles.Count; i++)
        {
            for (int j = 0; j < 6; j++)
            {

                Tile to = manager.tiles[i].direction(j);

                for (int k = 0; k < manager.tiles.Count; k++)
                {
                    if (to == manager.tiles[k])
                    {
                        graph.AddEdge(manager.tiles[i].Object, manager.tiles[k].Object);
                    }
                }
            }
        }
    }

    void Update()
    {
        graph.debugDraw();
    }
}

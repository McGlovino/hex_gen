﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Init : MonoBehaviour
{

    public GameObject HManager;
    public GameObject PController;
    
    void Start()
    {
        HManager.GetComponent<HexManager>().GenerateMap();
        HManager.GetComponent<WPManager>().GenerateLinks();
        PController.GetComponent<PlayerController>().Init();
    }
}
